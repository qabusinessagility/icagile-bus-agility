# ICAgile Business Agility Foundations
---

# ICAgile Business Agility Foundations

## Accredited course

![ICAgile course accreditation](images/accreditation.png)

This course has been accredited by ICAgile.

---

# ICAgile Business Agility Foundations

## ICAgile Business Agility Foundations accreditation

![ICAgile Business Agility Foundations - ICP-BAF badge](images/ICP-BAF.png)

People at every level and in practically every role in today's organizations are required to focus on customer delight, and bring innovation and continuous improvement into their work. This is a hard challenge and one that requires either building new muscles or limbering up muscles we may not use often. This highly experiential set of learning outcomes equips participants with new knowledge, tools, and techniques to implement immediately. The ICAgile Certified Professional in **Business Agility Foundations (ICP-BAF)** is intended to jumpstart the organizational and individual transformation towards a more responsive, value-driven reality.

---

# ICAgile Business Agility Foundations

## ICAgile Roadmap

![ICAgile learning roadmap](images/roadmap-old.png)

ICAgile offers both knowledge-based and competency-based certifications:
* Certifications that start with “ICAgile Professional Certifications (ICP)” - Knowledge-based certifications focusing on fulfillment of ICAgile learning objectives and an in-class demonstration of acquired knowledge. (Pictured as silver ribbons)

* Certifications that start with “ICAgile Expert Certifications (ICE)” - Competency-based certification requiring considerable field experience and a demonstration of competency in front of an expert panel. (Pictured as gold ribbons)



