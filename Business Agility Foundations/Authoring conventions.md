# Authoring conventions

**General**

Use American-English so that the materials can be published more widely. e.g. Cloud Academy.

Markdown tip for VS-Code. To preview the output, hold down **[Ctrl][Shift] + V**. Toggle back.



**Custom authoring tags**

[COPIED] - This means that the following passage was lifted verbatim and shouldn't be included as-is. It could remain as trainer notes or be rewritten for publication.


[DEPTH] - This is for the SME to inform the reader (designers or future trainers) how much time and design effort should be expended. Tee-shirt sizing. E.g. [DEPTH-Medium].


[TODO] - Author has left note to self or designers. It could be a reminder to provide some content.


[RESPONSES] - Where prompting learners to provide their connections and suggestions, SME can supply some "possible responses". Take care to provide the most important desired responses as this will guide the designers who will produce digital content. i.e. These possible responses should not be a random selection.

---